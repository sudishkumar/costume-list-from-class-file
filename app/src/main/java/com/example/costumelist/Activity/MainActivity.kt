package com.example.costumelist.Activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.Button
import android.widget.ListView
import com.example.costumelist.Adapter.StudentAdapter
import com.example.costumelist.DataManager.Data
import com.example.costumelist.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val listView = findViewById<ListView>(R.id.list_view)
        listView.adapter = StudentAdapter(this,Data.shared.studentsArrayList)

        listView.setOnItemClickListener { adapterView: AdapterView<*>, view1: View, position: Int, l: Long ->
            val intent = Intent(this,StudentsDetailsActivity::class.java)
            intent.putExtra("position", position)
            startActivity(intent)
        }

        val addNewStudentButton = findViewById<Button>(R.id.add_newStudent_btn)
        addNewStudentButton.setOnClickListener {
            val intent =Intent(this,AddNewStudentActivity::class.java)
            startActivity(intent)
        }
    }
}