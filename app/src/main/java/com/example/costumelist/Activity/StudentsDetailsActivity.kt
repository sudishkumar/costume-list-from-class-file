package com.example.costumelist.Activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.costumelist.DataManager.Data
import com.example.costumelist.R
import kotlinx.android.synthetic.main.activity_students_details.*

class StudentsDetailsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_students_details)

        val position = intent.extras?.getInt("selectedIndex")
        val students = Data.shared.studentsArrayList.get(position as Int)
        nameDe_Text.text = students.name
        rollDe_Text.text = students.rollNum
        numberDe_Text.text = students.mob.toString()
    }
}