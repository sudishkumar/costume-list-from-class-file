package com.example.costumelist.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewParent
import android.widget.BaseAdapter
import com.example.costumelist.Models.Students
import com.example.costumelist.R
import kotlinx.android.synthetic.main.costume_list_item.view.*

class StudentAdapter(private val context: Context, private val dataSource: ArrayList<Students> ): BaseAdapter() {
    private val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    override fun getCount(): Int {
       return dataSource.size
    }

    override fun getItem(p0: Int): Any {
       return dataSource[p0]
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getView(position: Int, viewGroup: View?, parent: ViewGroup?): View {
        val rowView =inflater.inflate(R.layout.costume_list_item, parent, false)

        rowView.name_textView.text = dataSource.get(position).name
//        rowView.roll_textView.text = dataSource.get(position).rollNum
//        rowView.number_textView.text = dataSource.get(position).mob?.toString()
        return rowView
    }
}