package com.example.costumelist.DataManager

import com.example.costumelist.Models.Students

class Data {
    val studentsArrayList = ArrayList<Students>()

    companion object {
        private var instance:Data? = null

        val shared:Data
            get(){
                if (instance == null){
                    instance = Data()
                    shared.studentsArrayList.add(Students("Arman","A1",6200023941))
                    shared.studentsArrayList.add(Students("Sudish","A2",6200213941))
                }

                return  instance!!
            }
    }
}