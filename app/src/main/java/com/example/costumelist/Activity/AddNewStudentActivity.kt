package com.example.costumelist.Activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.widget.Button
import android.widget.EditText
import com.example.costumelist.DataManager.Data
import com.example.costumelist.Models.Students
import com.example.costumelist.R

class AddNewStudentActivity : AppCompatActivity() {
     var data = Data
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_new_student)

        val addName = findViewById<EditText>(R.id.add_name)
        val addRoll = findViewById<EditText>(R.id.add_roll)
        val addNumber = findViewById<EditText>(R.id.add_number)
        val saveButton = findViewById<Button>(R.id.save_btn)

        saveButton.setOnClickListener {
            saveButton.isEnabled = false
            data.shared.studentsArrayList.add(Students(addName.text.toString(),addRoll.text.toString(),addNumber.text.toString().toLong()))
            this.finish()
        }
    }
}